module.exports = {
    apps: [{
        name: 'cs-update',
        script: 'app.js',
        args: 'one two',
        instances: 1,
        autorestart: true,
        exp_backoff_restart_delay: 100,
        watch: false,
        max_memory_restart: '1G',
        env: {
            NODE_ENV: 'production',
            PORT: 3333
        },
        env_production: {
            NODE_ENV: 'production',
            PORT: 3333
        }
    }]
};