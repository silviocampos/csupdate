const express = require('express')
const ControllerUpdater = require('./controllers/ControllerUpdater')
const morgan = require('morgan')

const app = express(),
    storage = require('./storage'),
    multer = require('multer')

const upload = multer({ storage })

app.use(morgan('dev'))

app.use(express.static('public'))

app.get('/update/:application/:version', ControllerUpdater.index)
app.post('/update/:application/:version', upload.single('file'), ControllerUpdater.update)
app.get('/download/:application/:version', ControllerUpdater.download)

app.listen(process.env.PORT || 3333,
    () => console.log("Service updater....")
)