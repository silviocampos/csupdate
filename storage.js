const multer = require('multer'),
    path = require('path'),
    fs = require('fs'),
    mime = require('mime')

const storage = multer.diskStorage({

    destination: async (req, file, cb) => {

        const { application } = req.params

        const pathApplication = `${path.resolve('public', application)}`

        if (!fs.existsSync(pathApplication))
            fs.mkdirSync(pathApplication)

        cb(null, pathApplication)
    },
    filename: (req, file, cb) => {

        const { version } = req.params
        const fileExtension = mime.getExtension(file.mimetype)
        const nameFile = `${version}.${fileExtension}`
        cb(null, nameFile);

    }
})

module.exports = storage;