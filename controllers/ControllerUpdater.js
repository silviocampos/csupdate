//MIGRAR ISSO PARA BANCO DE DADOS DEPOIS
const fs = require('fs')
const path = require('path')
const { version } = require('os')
const applications = fs.readdirSync(path.resolve('public'))


class ControllerUpdater {

    async index(req, res) {

        const { application, version } = req.params

        if (!applications.includes(application))
            return res.status('404').send({ error: "Application not found!" })

        const files = fs.readdirSync(path.resolve('public', application))

        if (files.length === 0)
            return res.send({ url: false })

        const [file] = files.map(file => Number(file.split('.')[0])).sort((a, b) => b - a);
        const currentVersion = file
        const appVersion = Number(version)

        if (appVersion < currentVersion)
            return res.send({ url: `download/${application}/${currentVersion}` })

        return res.send({ url: false })

    }

    async download(req, res) {

        const { version, application } = req.params
        const file = fs.existsSync(path.resolve('public', application, `${version}.exe`))

        if (!file)
            return res.status(404).send({ message: "Arquivo de instalção não encontrado." })

        return res.send({ url: req.protocol + '://' + req.get('host') + `/${application}/${version}.exe` })
    }

    async update(req, res) {
        return res.send({ upload: true })
    }

}

module.exports = new ControllerUpdater()